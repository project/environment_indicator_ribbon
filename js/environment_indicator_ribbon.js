/**
 * @file
 * Adds the environment specifics.
 */

(function ($, Drupal, drupalSettings, once) {
    'use strict';

    Drupal.behaviors.environmentIndicatorRibbon = {
        attach: function () {
            if (drupalSettings.hasOwnProperty('environment_indicator_ribbon')) {
                var name = drupalSettings.environment_indicator_ribbon.name;
                var fgcolor = drupalSettings.environment_indicator_ribbon.fgColor;
                var bgcolor = drupalSettings.environment_indicator_ribbon.bgColor;
                $(once('environmentIndicatorRibbon', 'body')).append('<div class="environment-indicator-ribbon" style="background-color: ' + bgcolor + '; color: ' + fgcolor + ';">' + name + '</div>');
            }
        }
    };

})(jQuery, Drupal, drupalSettings, once);
